-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `kategori` (`id`, `nama`) VALUES
(1,	'undangan'),
(2,	'pengumuman'),
(3,	'nota dinas'),
(4,	'pemberitahuan');

DROP TABLE IF EXISTS `surat`;
CREATE TABLE `surat` (
  `judulsurat` varchar(100) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nosurat` varchar(100) DEFAULT NULL,
  `kategori` int(11) NOT NULL,
  `tglarsip` date NOT NULL,
  `file` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `surat` (`judulsurat`, `id`, `nosurat`, `kategori`, `tglarsip`, `file`) VALUES
('undangan kerjabakti',	36,	'21021BD/KS01',	1,	'2021-11-21',	'resi.pdf'),
('Rapat RT',	37,	'21021BD/KS02',	1,	'2021-11-21',	'2021-SURAT PERNYATAAN_IJAZAH (1).pdf'),
('Rapat Karangtaruna',	38,	'21021BD/KS03',	4,	'2021-11-21',	'niali ku.pdf'),
('Rapat Karangtaruna 6',	39,	'21021BD/KS09',	1,	'2021-11-21',	'ukt smtr akhir.pdf');

-- 2021-11-21 13:27:14
