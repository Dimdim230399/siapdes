
<?php
	 include 'layout/header.php'
?>


	<!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
					<!--begin::Subheader-->
					<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
						<div
							class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
							<!--begin::Info-->
							<div class="d-flex align-items-center flex-wrap mr-2">
								<!--begin::Page Title-->
								<h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Dashboard</h5>
								<!--end::Page Title-->
								<!--begin::Actions-->
								<div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200">
								</div>
								<span class="text-muted font-weight-bold mr-4"> => </span>
								<a href="#" class="btn btn-light-warning font-weight-bolder btn-sm">BIODATA</a>
								<!--end::Actions-->
							</div>
							<!--end::Info-->
						</div>
					</div>
					<!--end::Subheader-->
				
					<!-- isi -->

							<div class="container">
								<!--begin::Card-->
								<div class="card card-custom gutter-b">
									<div class="card-header flex-wrap border-0 pt-6 pb-0">
										<div class="card-title">
											<h3 class="card-label">About Me
											<span class="d-block text-muted pt-2 font-size-sm">Aplikasi Ini Di Buat Oleh </span></h3>
										</div>
									
									</div>

                                        <!--begin::Body-->
                                    <div class="card-body d-flex flex-column">
                                    <div class="d-flex mb-9">
											<!--begin: Pic-->
											<div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
												<div class="symbol symbol-50 symbol-lg-120">
													<img src="assets/media/users/aku.jpeg" alt="image">
												</div>
												<div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
													<span class="font-size-h3 symbol-label font-weight-boldest">DMS</span>
												</div>
											</div>
											<!--end::Pic-->
											<!--begin::Info-->
											<div class="flex-grow-1">
												<!--begin::Title-->
												<div class="d-flex justify-content-between flex-wrap mt-1">
													<div class="d-flex mr-3">
														<a href="#" class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3">Dimas Setyo Nugroho</a>
														<a href="#">
															<i class="flaticon2-correct text-success font-size-h5"></i>
														</a>
													</div>
													<div class="my-lg-0 my-3">
														<a href="#" class="btn btn-sm btn-light-success font-weight-bolder text-uppercase mr-3">WA Me</a>
													</div>
												</div>
												<!--end::Title-->
												<!--begin::Content-->
												<div class="d-flex flex-wrap justify-content-between mt-1">
													<div class="d-flex flex-column flex-grow-1 pr-8">
														<div class="d-flex flex-wrap mb-4">
                                                            <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                                                <i class="flaticon2-calendar-3 mr-2 font-size-lg"></i>1931713119</a>
                                                                <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                                                <i class="flaticon2-new-email mr-2 font-size-lg"></i>dimassetyonugroho0@gmail.com</a>
															<a href="#" class="text-dark-50 text-hover-primary font-weight-bold">
															<i class="flaticon2-placeholder mr-2 font-size-lg"></i>20 November 2021</a>
														</div>
														<span class="font-weight-bold text-dark-50">Sistem Informasi Arsip Surat Desa Aplikasi Ini Dibuat</span>
														<span class="font-weight-bold text-dark-50"> Sebagai Penilaian Sertifikasi Pemrograman Poli Teknik Negri Malang</span>
													</div>
													
												</div>
												<!--end::Content-->
											</div>
											<!--end::Info-->
										</div>
                                    </div>
                                    <!--end::Body-->
			
								</div>
								<!--end::Card-->

							
							</div>
							<!--end::Container-->
					<!-- end isi -->
				
				</div>
				<!--end::Content-->
<?php
	 include 'layout/footer.php'
?>
